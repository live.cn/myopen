package com.myopen.base;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.config.ConstConfig;

public class BaseController extends Controller{
	
	public Record getSearchRecord(){
		
		Record record=new Record();
		
		Integer pageNumber=getParaToInt("page");
		if(pageNumber==null){
			pageNumber=1;
		}
		Integer pageSize=getParaToInt("limit");
		if(pageSize==null){
			pageSize=10;
		}
		if(pageSize>500){
			pageSize=500;//防止有人调用数据，每页最多500条数据
		}
		
		record.set("pageNumber", pageNumber);
		record.set("pageSize", pageSize);
		
		return record;
	}
	
	public Record getAdminUser(){
		return getSessionAttr(ConstConfig.SESSION_ADMIN_USER);
	}
}
