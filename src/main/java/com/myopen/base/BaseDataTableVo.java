package com.myopen.base;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Page;

public class BaseDataTableVo {

	private String code="0"; //默认为0，表示执行成功！
	private String msg="";
	private int count=0;
	private List<Object> data=new ArrayList<Object>();
	
	public BaseDataTableVo(String code,String msg) {
		this.code=code;
		this.msg=msg;
	}
	
	public BaseDataTableVo(Page page) {
		this.count=page.getTotalRow();
		this.data=page.getList();
	}

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<Object> getData() {
		return data;
	}
	public void setData(List<Object> data) {
		this.data = data;
	}}
