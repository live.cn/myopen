package com.myopen.base;

public class BaseVo {

	private String code="0"; //默认为0，表示执行成功！
	private Object data;
	private Object extendData;//一个数据对象不够用时，可以用这个
	private String msg;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Object getData() {
		return data;
	}
	public Object getExtendData() {
		return extendData;
	}
	public void setExtendData(Object extendData) {
		this.extendData = extendData;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
