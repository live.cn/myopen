package com.myopen.cachestatic;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Record;
import com.myopen.model.DicType;

/**
 * 静态缓存
 * @author wangsanfei
 *
 */
public class DicCacheStatic {

	/**
	 * 字典缓存
	 */
	private static List<Record> dicTypes=null;
	
	public static void setDicTypes(){
		dicTypes=DicType.dao.getAllDicTypeAndDicInfo();
	}
	public static List<Record> getDicTypes(){
		if(dicTypes==null) {
			setDicTypes(); 
		}
		return dicTypes;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Record> getDicInfos(String dicTypeCode){
		List<Record> dicInfos=new ArrayList<>();
		for (Record dicType : getDicTypes()) {
			if(dicTypeCode.equals(dicType.getStr("code"))) {
				dicInfos = (List<Record>)dicType.get("dicInfos");
				break;
			}
		}
		return dicInfos;
	}
	
	public static Record getDicInfo(String dicTypeCode,String dicInfoCode) {
		List<Record> dicInfos=getDicInfos(dicTypeCode);
		for (Record dicInfo : dicInfos) {
			if(dicInfoCode.equals(dicInfo.getStr("code"))) {
				return dicInfo;
			}
		}
		return null;
	}
	
	////////////////////////////////////////////////
}
