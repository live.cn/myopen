package com.myopen.config;

import com.alibaba.druid.filter.stat.StatFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.cron4j.Cron4jPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;
import com.myopen.cachestatic.DicCacheStatic;
import com.myopen.directive.AuthDirective;
import com.myopen.interceptor.LogInterceptor;
import com.myopen.model.DicInfo;
import com.myopen.model.DicType;
import com.myopen.model.Log;
import com.myopen.model.Resource;
import com.myopen.model.Role;
import com.myopen.model.Task;
import com.myopen.model.User;
import com.myopen.model.Website;
import com.myopen.model.WebsiteColumn;
import com.myopen.model.WebsiteContent;
import com.myopen.model.WebsiteMaterial;
import com.myopen.model.WebsitePage;
import com.myopen.route.AdminRoutes;
import com.myopen.route.FrontRoutes;

/**
 * 
 * @author wangsanfei
 *
 */
public class AppConfig extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		PropKit.use("config.txt");
		me.setDevMode(PropKit.getBoolean("devMode", true));
		
	}

	@Override
	public void configEngine(Engine me) {
		me.addSharedFunction("/admin/common/layout.html");
		me.addSharedFunction("/common/dic_get_select.html");
		me.addSharedFunction("/common/dic_get_value.html");
		
		me.addSharedObject("StrKit", new StrKit());
		me.addSharedObject("ConstConfig", new ConstConfig());
		me.addSharedObject("User",new User());
		me.addSharedObject("DicCacheStatic",new DicCacheStatic());
		
		me.addDirective("auth", new AuthDirective());
		
	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new LogInterceptor());
		me.add(new SessionInViewInterceptor());
		
	}

	@Override
	public void configPlugin(Plugins me) {
		DruidPlugin dp = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password"));
		dp.addFilter(new StatFilter()); // 添加 StatFilter 才会有统计数据 
		me.add(dp);
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dp); 
		arp.addMapping("t_user", User.class);
		arp.addMapping("t_role", Role.class);
		arp.addMapping("t_resource", Resource.class);
		arp.addMapping("t_log", Log.class);
		arp.addMapping("t_dic_type", DicType.class);
		arp.addMapping("t_dic_info", DicInfo.class);
		arp.addMapping("t_task", Task.class);
		arp.addMapping("t_website", Website.class);
		arp.addMapping("t_website_material", WebsiteMaterial.class);
		arp.addMapping("t_website_column", WebsiteColumn.class);
		arp.addMapping("t_website_content", WebsiteContent.class);
		arp.addMapping("t_website_page", WebsitePage.class);
		me.add(arp);
		
		Cron4jPlugin cp=new Cron4jPlugin();
		
	}

	@Override
	public void configRoute(Routes me) {
		me.add(new FrontRoutes()); // 前端路由
		me.add(new AdminRoutes()); // 后端路由
	}
	
	/**
	 * 启动前
	 */
	public void beforeJFinalStop(){
		
	};
	
	/**
	 * 启动后
	 */
	public void afterJFinalStart(){
		
	};
	
	/**
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 * 
	 * 使用本方法启动过第一次以后，会在开发工具的 debug、run config 中自动生成
	 * 一条启动配置，可对该自动生成的配置再添加额外的配置项，例如 VM argument 可配置为：
	 * -XX:PermSize=64M -XX:MaxPermSize=256M
	 */
	public static void main(String[] args) {
		/**
		 * 特别注意：Eclipse 之下建议的启动方式
		 */
		JFinal.start("src/main/webapp", 8000, "/", 5);
		
		/**
		 * 特别注意：IDEA 之下建议的启动方式，仅比 eclipse 之下少了最后一个参数
		 */
		// JFinal.start("src/main/webapp", 80, "/");
	}

}
