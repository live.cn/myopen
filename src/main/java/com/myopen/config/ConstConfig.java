package com.myopen.config;

public class ConstConfig {

	public static final String SESSION_USER="SESSION_USER";
	
	
	public static final String SESSION_ADMIN_USER="SESSION_ADMIN_USER";//后台管理用户session名称
	public static final String SESSION_ADMIN_ROLE="SESSION_ADMIN_ROLE";//后台管理session角色名称
	public static final String SESSION_ADMIN_RESOURCE_URLS="SESSION_ADMIN_RESOURCE_URLS";//后台管理资源对应的urls
	public static final String SESSION_ADMIN_RESOURCE_CODES="SESSION_ADMIN_RESOURCE_CODES";//后台管理资源对应的codes
	public static final String SESSION_ADMIN_MENUS="SESSION_ADMIN_MENUS";//后台管理菜单
	public static final String SESSION_MENU_OPEN_CODE="SESSION_MENU_OPEN_CODE";//后台管理打开的菜单，用于标记前台
	
	
	
	public static final String ACTION_TYPE="ACTION_TYPE";//操作类型，有固定的，新增，编辑，详情，考虑到简单，暂时只有新增，编辑，详情
	public static final String ACTION_TYPE_ADD="ACTION_TYPE_ADD";
	public static final String ACTION_TYPE_EDIT="ACTION_TYPE_EDIT";
	public static final String ACTION_TYPE_VIEW="ACTION_TYPE_VIEW";
	
	
}
