package com.myopen.config;

public enum ErrorConfig {
	
	//用户模块，10001-10500
    err_10001("10001","") ,
   
    
    //资源模块，10501-11000
    err_10501("err_10501","") ,// 方便加注释
    
    ////我的站点模块，11001-11500
    err_11001("err_11001","站点名称不能为空！"),
    ;
     
   private String msg;  
   private String code;  
     
   private ErrorConfig(String code,String msg) { 
       this.code=code;  
       this.msg=msg;  
   }
   
   public String getMsg(){   
       return this.msg;  
   }  
   public String getCode() {  
     return this.code;  
   }  
	
}
