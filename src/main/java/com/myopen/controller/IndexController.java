package com.myopen.controller;

import com.jfinal.core.ActionKey;
import com.myopen.base.BaseController;

public class IndexController extends BaseController{

	public void index(){
		redirect("/admin");
	}
	
	@ActionKey("/captcha")
	public void captcha() {
		renderCaptcha();
	}
}
