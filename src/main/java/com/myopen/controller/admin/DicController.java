package com.myopen.controller.admin;

import java.util.List;

import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseVo;
import com.myopen.model.DicInfo;
import com.myopen.model.DicType;

public class DicController extends BaseController{

	@ActionKey("/admin/dic")
	public void index(){
		render("/admin/dic/index.html");
	}
	
	@ActionKey("/admin/dicType/getAll")
	public void getALlDicTypes() {
		List<Record> list=DicType.dao.getAllDicType();
		
		BaseVo baseVo=new BaseVo();
		baseVo.setData(list);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/dicType/save")
	public void dicTypeSave() {
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		DicType vo=getModel(DicType.class,"",true);
		
		//验证CODE是否存在
		
		vo=DicType.dao.save(vo, currUser);
		
		Record data=new Record();
		data.set("vo", vo);
		baseVo.setData(data);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/dicType/del")
	public void dicTypeDel() {
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		
		String id=getPara("id");
		DicType vo=DicType.dao.remove(id,currUser);
		
		Record data=new Record();
		data.set("vo", vo);
		baseVo.setData(data);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/dicInfo/getAll")
	public void getALlDicInfos() {
		String dicTypeId=getPara("dicTypeId");
		List<Record> list=DicInfo.dao.getAllDicInfo(dicTypeId);
		
		BaseVo baseVo=new BaseVo();
		baseVo.setData(list);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/dicInfo/save")
	public void dicInfoSave() {
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		DicInfo vo=getModel(DicInfo.class,"",true);
		
		//验证CODE是否存在
		
		vo=DicInfo.dao.save(vo, currUser);
		
		Record data=new Record();
		data.set("vo", vo);
		baseVo.setData(data);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/dicInfo/del")
	public void dicInfoDel() {
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		
		String id=getPara("id");
		DicInfo vo=DicInfo.dao.remove(id,currUser);
		
		Record data=new Record();
		data.set("vo", vo);
		baseVo.setData(data);
		
		renderJson(baseVo);
	}
}
