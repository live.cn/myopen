package com.myopen.controller.admin;

import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseDataTableVo;
import com.myopen.model.Log;

public class LogController extends BaseController{

	@ActionKey("/admin/log")
	public void index(){
		render("/admin/log/index.html");
	}
	
	@ActionKey("/admin/log/pagequery")
	public void pagequery(){
		
		Record record=getSearchRecord();
		
		Page<Record> page=Log.dao.pagequery(record);
		
		BaseDataTableVo tableVo=new BaseDataTableVo(page);
		
		renderJson(tableVo);
	}
}
