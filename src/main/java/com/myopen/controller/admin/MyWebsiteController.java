package com.myopen.controller.admin;

import com.jfinal.core.ActionKey;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseDataTableVo;
import com.myopen.model.Website;
import com.myopen.model.WebsiteColumn;
import com.myopen.model.WebsiteContent;

public class MyWebsiteController extends BaseController{

	@ActionKey("/admin/myWebsiteContent")
	public void myCmsWebsiteContent() {
		render("/admin/myWebsite/myWebsiteContent/index.html");
	}
	
	@ActionKey("/admin/myWebsiteContent/pagequery")
	public void myCmsWebsiteContentPageQuery() {
		
		Record record=getSearchRecord();//会自动组装通用的参数
		record.set("email", getPara("email"));
		record.set("cellphone", getPara("cellphone"));
		Page<Record> page=WebsiteContent.dao.pagequery(record);
		
		BaseDataTableVo tableVo=new BaseDataTableVo(page);
		renderJson(tableVo);
	}
	
	@ActionKey("/admin/myWebsite/publish")
	public void myCmsPublish(){
		
		//Record currUser=getAdminUser();
		Record websiteColumn=new Record();
		String module=null;
		String websiteColumnId=getPara("websiteColumnId");
		if(!StrKit.isBlank(websiteColumnId)) {
			websiteColumn= WebsiteColumn.dao.getRecordById(websiteColumnId);
			module=websiteColumn.getStr("module");
		}
		
		setAttr("websiteColumnId", websiteColumnId);
		setAttr("module", module);
		setAttr("vo", new Record());
		if("CMS_MODULE_00".equals(module)) {
			render("/admin/myWebsite/myWebsitePublish/index_00.html");
		}else if("CMS_MODULE_01".equals(module)){
			render("/admin/myWebsite/myWebsitePublish/index_01.html");
		}else {
			render("/admin/myWebsite/myWebsitePublish/index.html");
		}
	}
	
	@ActionKey("/admin/myWebsite/publish/save")
	public void myCmsPublishSave(){
		
		Record currUser=getAdminUser();
		
		String websiteColumnId=getPara("websiteColumnId");
		String title=getPara("title");
		String attribute=getPara("attribute");
		Record websiteColumn=WebsiteColumn.dao.getRecordById(websiteColumnId);
		Record website=Website.dao.getRecordById(websiteColumn.getStr("websiteId"));
		
		
		WebsiteContent vo=new WebsiteContent();
		vo.set("websiteId", website.getStr("id"));
		vo.set("websiteColumnId", websiteColumnId);
		vo.set("module", websiteColumn.getStr("module"));
		vo.set("title", title);
		vo.set("attribute", attribute);
		
		WebsiteContent.dao.save(vo, currUser);
		
	}
	
}
