package com.myopen.controller.admin;

import java.util.List;

import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseVo;
import com.myopen.model.Resource;

public class ResourceController extends BaseController{

	@ActionKey("/admin/resource")
	public void index(){
		render("/admin/resource/index.html");
	}
	
	@ActionKey("/admin/resource/tree")
	public void tree(){
		
		
		List<Record> treeJson=Resource.dao.getTree();
		
		BaseVo baseVo=new BaseVo();
		baseVo.setData(treeJson);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/resource/detail")
	public void detail(){
		String id=getPara("id");
		
		BaseVo baseVo=new BaseVo();
		Record resource=Resource.dao.getRecordById(id);
		
		Record data=new Record();
		data.set("vo", resource);
		data.set("resourceTree", Resource.dao.getTree());
		
		baseVo.setData(data);
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/resource/save")
	public void save(){
		
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		Resource vo=getModel(Resource.class,"");
		
		boolean flag=true;
		Record record=Resource.dao.getRecordByCode(vo.getStr("code"));
		if(record!=null) {
			//排除同一条数据
			if(!record.getStr("id").equals(vo.getStr("id"))) {
				baseVo.setCode("1");//资源代码不能重复
				flag=false;
			}
		}
		
		if(flag) {
			vo=Resource.dao.save(vo, currUser);
			Record data=new Record();
			data.set("vo", vo);
			baseVo.setData(data);
		}
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/resource/delete")
	public void delete(){
		
		Record currUser=getAdminUser();
		String id=getPara("id");
		Resource.dao.remove(id,currUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
}
