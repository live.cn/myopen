package com.myopen.controller.admin;

import java.util.List;

import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseDataTableVo;
import com.myopen.base.BaseVo;
import com.myopen.config.ConstConfig;
import com.myopen.model.Role;

public class RoleController extends BaseController{

	@ActionKey("/admin/role")
	public void index(){
		render("/admin/role/index.html");
	}
	
	@ActionKey("/admin/role/pagequery")
	public void pagequery(){
		
		Record record=getSearchRecord();
		record.set("name", getPara("name"));
		record.set("code", getPara("code"));
		
		Page<Record> page=Role.dao.pagequery(record);
		
		BaseDataTableVo tableVo=new BaseDataTableVo(page);
		
		renderJson(tableVo);
	}
	
	@ActionKey("/admin/role/add")
	public void add(){
		
		setAttr("vo", new Record());//有时候需要事先编一个号，这样比较方便
		setAttr(ConstConfig.ACTION_TYPE, ConstConfig.ACTION_TYPE_ADD);
		render("/admin/role/detail.html");
	}
	
	@ActionKey("/admin/role/view")
	public void view(){
		
		String id=getPara("id");
		Record vo=Role.dao.findById(id).toRecord();
		
		setAttr("vo", vo);
		setAttr(ConstConfig.ACTION_TYPE, ConstConfig.ACTION_TYPE_VIEW);
		render("/admin/role/detail.html");
	}
	
	@ActionKey("/admin/roleResource/detail")
	public void roleResource_detail(){
		String id=getPara("id");
		
		BaseVo baseVo=new BaseVo();
		List<Record> roleResources=Role.dao.getTreeRoleResources(id);
		
		baseVo.setData(roleResources);
		renderJson(baseVo);
	}
	
	
	@ActionKey("/admin/role/save")
	public void save(){
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		Role vo=getModel(Role.class,"");
		
		vo=Role.dao.save(vo,currUser);
		
		Record data=new Record();
		data.set("vo", vo);
		
		baseVo.setData(data);
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/roleResource/save")
	public void roleResource_save(){
		
		Record currUser=getAdminUser();
		String roleId=getPara("authRoleId");
		String[] resourceIds=getParaValues("resourceIds[]");
		System.out.println(getParaMap());
		BaseVo baseVo=new BaseVo();
		
		Role.dao.updateRoleResource(roleId, resourceIds, currUser);
		
		Record data=new Record();
		
		baseVo.setData(data);
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/role/startUsing")
	public void startUsing(){
		
		Record currrUser=getAdminUser();
		String id=getPara("id");
		Role.dao.changeStatus(id, "ROLE_STATUS_00", currrUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/role/forbidden")
	public void forbidden(){
		
		Record currrUser=getAdminUser();
		String id=getPara("id");
		Role.dao.changeStatus(id, "ROLE_STATUS_01", currrUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
	
}
