package com.myopen.controller.admin;

import java.util.List;

import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseDataTableVo;
import com.myopen.base.BaseVo;
import com.myopen.config.ConstConfig;
import com.myopen.model.Role;
import com.myopen.model.Task;

/**
 * 
 * @author wangsanfei
 *
 */
public class TaskController extends BaseController {

	@ActionKey("/admin/task")
	public void index(){
		render("/admin/task/index.html");
	}
	
	@ActionKey("/admin/task/pagequery")
	public void pagequery(){
		
		Record record=getSearchRecord();//会自动组装通用的参数
		Page<Record> page=Task.dao.pagequery(record);
		
		BaseDataTableVo tableVo=new BaseDataTableVo(page);
		renderJson(tableVo);
	}
	
	@ActionKey("/admin/task/add")
	public void add(){
		List<Record> roles=Role.dao.getAll();
		
		setAttr("roles", roles);
		setAttr("vo", new Record());//有时候需要事先编一个号，这样比较方便
		setAttr(ConstConfig.ACTION_TYPE, ConstConfig.ACTION_TYPE_ADD);
		render("/admin/task/detail.html");
	}
	
	@ActionKey("/admin/task/view")
	public void view(){
		String id=getPara("id");
		Record vo=Task.dao.findById(id).toRecord();
		List<Record> roles=Role.dao.getAll();
		
		setAttr("roles", roles);
		setAttr("vo", vo);
		setAttr(ConstConfig.ACTION_TYPE, ConstConfig.ACTION_TYPE_VIEW);
		render("/admin/task/detail.html");
	}
	
	
	@ActionKey("/admin/task/detail")
	public void detail(){
		
		String id=getPara("id");
		
		BaseVo baseVo=new BaseVo();
		Record task=Task.dao.getRecordById(id);
		
		Record data=new Record();
		data.set("vo", task);
		
		baseVo.setData(data);
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/task/save")
	public void save(){
		
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		Task task=getModel(Task.class,"");
		
		boolean flag=true;
		//首先验证下参数
		
		if(flag) {
			
			String jobName=task.getStr("jobName");
			String jobGroupName=task.getStr("jobGroup");
			String triggerName=task.getStr("triggerName");
			String triggerGroupName=task.getStr("triggerGroupName");
			String description=task.getStr("description");
			String jobClass=task.getStr("jobClass");
			String cronExpression=task.getStr("cronExpression");
			
			Task.dao.addTask(jobName,jobGroupName,triggerName,triggerGroupName,description,jobClass,cronExpression,currUser);
			Record data=new Record();
			data.set("vo", task);
			baseVo.setData(data);
		}
		
		
		renderJson(baseVo);
		
	}
}
