package com.myopen.controller.admin;

import java.util.List;

import com.jfinal.core.ActionKey;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseDataTableVo;
import com.myopen.base.BaseVo;
import com.myopen.config.ConstConfig;
import com.myopen.model.Role;
import com.myopen.model.User;

public class UserController extends BaseController{

	@ActionKey("/admin/user")
	public void index(){
		render("/admin/user/index.html");
	}
	
	@ActionKey("/admin/user/pagequery")
	public void pagequery(){
		
		Record record=getSearchRecord();//会自动组装通用的参数
		record.set("searchContent", getPara("searchContent"));
		record.set("status", getPara("status"));
		Page<Record> page=User.dao.pagequery(record);
		
		BaseDataTableVo tableVo=new BaseDataTableVo(page);
		renderJson(tableVo);
	}
	
	@ActionKey("/admin/user/add")
	public void add(){
		List<Record> roles=Role.dao.getAll();
		
		setAttr("roles", roles);
		setAttr("vo", new Record());//有时候需要事先编一个号，这样比较方便
		setAttr(ConstConfig.ACTION_TYPE, ConstConfig.ACTION_TYPE_ADD);
		render("/admin/user/detail.html");
	}
	
	@ActionKey("/admin/user/view")
	public void view(){
		String id=getPara("id");
		Record vo=User.dao.findById(id).toRecord();
		List<Record> roles=Role.dao.getAll();
		
		setAttr("roles", roles);
		setAttr("vo", vo);
		setAttr(ConstConfig.ACTION_TYPE, ConstConfig.ACTION_TYPE_VIEW);
		render("/admin/user/detail.html");
	}
	
	
	@ActionKey("/admin/user/detail")
	public void detail(){
		
		String id=getPara("id");
		
		BaseVo baseVo=new BaseVo();
		List<Record> roles=Role.dao.getAll();
		Record user=User.dao.getRecordById(id);
		
		Record data=new Record();
		data.set("vo", user);
		data.set("roles", roles);
		
		baseVo.setData(data);
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/user/save")
	public void save(){
		
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		User user=getModel(User.class,"");
		
		boolean flag=true;
		
		
		if(!StrKit.isBlank(user.getStr("cellphone"))) {
			Record record=User.dao.getRecordByCellphone(user.getStr("cellphone"));
			if(record!=null) {
				if(!record.getStr("id").equals(user.getStr("id"))) {
					flag=false;
					baseVo.setCode("2");
				}
			}
		}
		
		if(!StrKit.isBlank(user.getStr("email"))) {
			Record record=User.dao.getRecordByEmail(user.getStr("email"));
			if(record!=null) {
				if(!record.getStr("id").equals(user.getStr("id"))) {
					flag=false;
					baseVo.setCode("1");
				}
			}
		}
		
		if(!StrKit.isBlank(user.getStr("username"))) {
			Record record=User.dao.getRecordByUsername(user.getStr("username"));
			if(record!=null) {
				if(!record.getStr("id").equals(user.getStr("id"))) {
					flag=false;
					baseVo.setCode("3");
				}
			}
		}
		
		if(flag) {
			user=User.dao.save(user,currUser);
			Record data=new Record();
			data.set("vo", user);
			baseVo.setData(data);
		}
		
		
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/user/startUsing")
	public void startUsing(){
		
		Record currUser=getAdminUser();
		String id=getPara("id");
		User.dao.changeStatus(id, "USER_STATUS_00", currUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/user/forbidden")
	public void forbidden(){
		
		Record currUser=getAdminUser();
		String id=getPara("id");
		User.dao.changeStatus(id, "USER_STATUS_01", currUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/user/delete")
	public void delete(){
		
		Record currUser=getAdminUser();
		String id=getPara("id");
		User.dao.remove(id,currUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
	
}
