package com.myopen.controller.admin;

import java.util.List;

import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseVo;
import com.myopen.model.WebsiteColumn;

public class WebsiteColumnController extends BaseController{
	
	@ActionKey("/admin/myWebsiteColumn")
	public void index(){
		render("/admin/myWebsite/myWebsiteColumn/index.html");
	}
	
	@ActionKey("/admin/myWebsiteColumn/tree")
	public void tree(){
		
		String module=getPara("module");
		List<Record> treeJson=WebsiteColumn.dao.getTree(module);
		
		BaseVo baseVo=new BaseVo();
		baseVo.setData(treeJson);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/myWebsiteColumn/detail")
	public void detail(){
		String id=getPara("id");
		
		BaseVo baseVo=new BaseVo();
		Record resource=WebsiteColumn.dao.getRecordById(id);
		
		Record data=new Record();
		data.set("vo", resource);
		
		baseVo.setData(data);
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/myWebsiteColumn/save")
	public void save(){
		
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		WebsiteColumn vo=getModel(WebsiteColumn.class,"",true);
		
		
		vo=WebsiteColumn.dao.save(vo, currUser);
		Record data=new Record();
		data.set("vo", vo);
		baseVo.setData(data);
		
		renderJson(baseVo);
	}
	
	@ActionKey("/admin/myWebsiteColumn/delete")
	public void delete(){
		
		Record currUser=getAdminUser();
		String id=getPara("id");
		WebsiteColumn.dao.remove(id,currUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
}
