package com.myopen.controller.admin;

import com.jfinal.aop.Before;
import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseDataTableVo;
import com.myopen.base.BaseVo;
import com.myopen.model.Website;
import com.myopen.validator.MyWebsiteValidator;

public class WebsiteController extends BaseController{

	public void index() {
		render("/admin/website/index.html");
	}
	
	@ActionKey("/admin/website/pagequery")
	public void pagequery(){
		
		Record record=getSearchRecord();//会自动组装通用的参数
		record.set("email", getPara("email"));
		record.set("cellphone", getPara("cellphone"));
		Page<Record> page=Website.dao.pagequery(record);
		
		BaseDataTableVo tableVo=new BaseDataTableVo(page);
		renderJson(tableVo);
	}
	
	@ActionKey("/admin/website/startUsing")
	public void startUsing(){
		
		Record currUser=getAdminUser();
		String id=getPara("id");
		Website.dao.changeStatus(id, "WEBSITE_STATUS_00", currUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
	
	@ActionKey("/admin/website/forbidden")
	public void forbidden(){
		
		Record currUser=getAdminUser();
		String id=getPara("id");
		Website.dao.changeStatus(id, "WEBSITE_STATUS_01", currUser);
		
		BaseVo baseVo=new BaseVo();
		renderJson(baseVo);
		
	}
	
	
	@ActionKey("/admin/myWebsite")
	public void myWebsite(){
		
		Record currUser=getAdminUser();
		Record myWebsite=Website.dao.getMyWebsite(currUser.getStr("id"));
		
		setAttr("vo", myWebsite);
		render("/admin/myWebsite/index.html");
		
	}
	
	@Before(MyWebsiteValidator.class)
	@ActionKey("/admin/myWebsite/save")
	public void myWebsiteSave(){
		
		BaseVo baseVo=new BaseVo();
		Record currUser=getAdminUser();
		Website vo=getModel(Website.class,"");
		
		//进行一些必要的验证,这个验证放在了MyWebsiteValidator里面
		vo=Website.dao.save(vo,currUser);
		
		baseVo.setCode("0");
	}
	
}
