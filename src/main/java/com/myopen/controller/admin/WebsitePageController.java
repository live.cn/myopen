package com.myopen.controller.admin;

import com.jfinal.core.ActionKey;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.base.BaseController;
import com.myopen.base.BaseDataTableVo;
import com.myopen.model.WebsitePage;

public class WebsitePageController extends BaseController{

	public void index() {
		render("/admin/websitePage/index.html");
	}
	
	@ActionKey("/admin/websitePage/pagequery")
	public void pagequery(){
		
		Record record=getSearchRecord();//会自动组装通用的参数
		record.set("email", getPara("email"));
		record.set("cellphone", getPara("cellphone"));
		Page<Record> page=WebsitePage.dao.pagequery(record);
		
		BaseDataTableVo tableVo=new BaseDataTableVo(page);
		renderJson(tableVo);
	}
}
