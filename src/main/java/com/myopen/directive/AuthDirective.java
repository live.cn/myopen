package com.myopen.directive;

import java.io.Writer;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.jfinal.plugin.activerecord.Record;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.stat.Scope;
import com.myopen.config.ConstConfig;

public class AuthDirective extends Directive{

	@Override
	@SuppressWarnings("unchecked")
	public void exec(Env env, Scope scope, Writer writer) {
		Object[] obj=this.exprList.evalExprList(scope);
		HttpSession session=(HttpSession)obj[0];
		String authCode=(String)obj[1];
		Record currUser=(Record)session.getAttribute(ConstConfig.SESSION_ADMIN_USER);
		Map<String,Record> sessionResourceCodes=(Map<String,Record>)currUser.get(ConstConfig.SESSION_ADMIN_RESOURCE_CODES);
		if(sessionResourceCodes.get(authCode)!=null) {
			stat.exec(env, scope, writer);
		}
	}
	
	public boolean hasEnd() {
		return true;
	}

}
