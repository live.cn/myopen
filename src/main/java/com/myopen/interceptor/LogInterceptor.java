package com.myopen.interceptor;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.config.ConstConfig;
import com.myopen.util.IpUtil;
import com.myopen.util.UUIDUtils;

public class LogInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation arg0) {
		Date startTime=new Date();
		
		arg0.invoke();
		
		String controllerKey=arg0.getControllerKey();
		HttpServletRequest request=arg0.getController().getRequest();
		HttpSession session=arg0.getController().getSession();
		
		Record user=(Record)session.getAttribute(ConstConfig.SESSION_USER);
		
		if(user!=null) {
			Record bo=new Record();
			Date endTime=new Date();
			bo.set("id", UUIDUtils.getUUID());
			bo.set("createTime", new Date());
			bo.set("updateTime", new Date());
			bo.set("creator", user.getStr("id"));
			bo.set("updator", user.getStr("id"));
			
			bo.set("system", "");
			bo.set("module", "");
			bo.set("auth", 0);
			bo.set("ip", IpUtil.getRealIp(request));
			bo.set("startTime", startTime);
			bo.set("endTime", endTime);
			bo.set("actionTime", endTime.getTime()-startTime.getTime());
			bo.set("method", request.getMethod());
			bo.set("referer", request.getHeader("referer"));
			bo.set("url", controllerKey);
			
			Db.save("t_log", bo);
		}
		
		
	}

}
