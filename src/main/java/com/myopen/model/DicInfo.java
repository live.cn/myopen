package com.myopen.model;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.util.UUIDUtils;

public class DicInfo extends Model<DicInfo>{

	private static final long serialVersionUID = -5051324043885531231L;
	public static final DicInfo dao = new DicInfo();
	
	public DicInfo save(DicInfo vo,Record currUser) {
		DicInfo bo=null;
		if(StrKit.isBlank(vo.getStr("id"))){
			bo=new DicInfo();
			
			bo.set("id", UUIDUtils.getUUID());
			bo.set("createTime", new Date());
			bo.set("updateTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updator", currUser.getStr("id"));
			bo.set("dicTypeId", vo.getStr("dicTypeId"));
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("px", vo.getInt("px"));
			
			bo.save();
		}else{
			bo=DicInfo.dao.findById(vo.getStr("id"));
			
			bo.set("creator", currUser.getStr("id"));
			bo.set("updator", currUser.getStr("id"));
			bo.set("dicTypeId", vo.getStr("dicTypeId"));
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("px", vo.getInt("px"));

			bo.update();
		}
		
		return bo;
	}
	
	public List<Record> getAllDicInfo(String dicTypeId){
		String sqlDicInfo="select * from t_dic_info t where t.removed='0' and t.dicTypeId=? order by t.px";
		return Db.find(sqlDicInfo,dicTypeId);
	}
	
	public DicInfo remove(String id,Record currUser) {
		DicInfo bo=DicInfo.dao.findById(id);
		bo.set("removed", "1");
		bo.set("updateTime", new Date());
		bo.set("updator", currUser.getStr("id"));
		bo.update();
		return bo;
	}
}
