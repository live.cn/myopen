package com.myopen.model;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

public class Log extends Model<Log>{

	private static final long serialVersionUID = -6408995522291184390L;
	public static final Log dao=new Log();
	
	public Page<Record> pagequery(Record record){
		StringBuffer sqlExceptSelect=new StringBuffer("from t_log t "
				+ " left join t_user u on t.creator=u.id"
				+ " where t.removed='0' ");
		List<Object> list=new ArrayList<Object>();
		
		sqlExceptSelect.append(" order by t.createTime desc");
		return Db.paginate(record.getInt("pageNumber"),record.getInt("pageSize"),"select t.*,u.username,u.nickname,u.email,u.cellphone ",sqlExceptSelect.toString(),list.toArray());		
	}
}
