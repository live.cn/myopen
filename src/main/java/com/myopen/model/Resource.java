package com.myopen.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.util.UUIDUtils;

public class Resource extends Model<Resource>{

	private static final long serialVersionUID = -629586816717850618L;
	public static final Resource dao = new Resource();
	
	public static String formatUrl(String url){
		if(!url.startsWith("/")){
			url="/"+url;
		}
		if(!url.endsWith("/")){
			url=url+"/";
		}
		return url;
	}
	
	public List<Record> getRecordsByRoleId(String roleId){
		String sql="select r.* from t_resource r "
				+ "inner join t_role_resource rr on r.id=rr.resourceId and rr.roleId=? "
				+ "where r.removed='0' and rr.removed='0' order by r.px ";
		return Db.find(sql,roleId);
	}
	
	public List<Record> getAllResources(){
		String sql="select * from t_resource t where t.removed='0' order by t.px ";
		return Db.find(sql);
	}
	
	public Record getRecordById(String id){
		return Db.findById("t_resource", id);
	}
	
	public Record getRecordByCode(String code){
		String sql="select * from t_resource t where t.removed='0' and t.code=? order by t.px ";
		return Db.findFirst(sql,code);
	}
	
	
	private void getChild(Record parent,List<Record> list){
		List<Record> childs=new ArrayList<Record>();
		for (Record record : list) {
			if(parent.getStr("id").equals(record.getStr("parentId"))){
				
				record.set("spread", true);
				
				getChild(record,list);
				childs.add(record);
			}
		}
		
		if(childs.size()>0){
			parent.set("children", childs);
		}
		
	}
	public List<Record> getTree(){
		String psql="select * from t_resource t where (t.parentId is null or t.parentId='') and t.removed='0' order by t.px ";
		String lsql="select * from t_resource t where 1=1 and t.removed='0' order by t.px";
		List<Record> parents=Db.find(psql);
		List<Record> list=Db.find(lsql);
		
		for (Record record : parents) {
			
			record.set("spread", true);
			
			getChild(record,list);
		}
		
		return parents;
	}
	
	public Resource save(Resource vo,Record currUser){
		
		Resource bo=null;
		if(StrKit.isBlank(vo.getStr("id"))){
			bo=new Resource();
			
			bo.set("id", UUIDUtils.getUUID());
			bo.set("createTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("url", vo.getStr("url"));
			bo.set("visibility", vo.getStr("visibility"));
			bo.set("px", vo.getInt("px"));
			bo.set("parentId", vo.getStr("parentId"));
			bo.set("comment", vo.getStr("comment"));
			
			bo.save();
		}else{
			bo=Resource.dao.findById(vo.getStr("id"));
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("url", vo.getStr("url"));
			bo.set("visibility", vo.getStr("visibility"));
			bo.set("px", vo.getInt("px"));
			bo.set("parentId", vo.getStr("parentId"));
			bo.set("comment", vo.getStr("comment"));

			bo.update();
		}
		
		return bo;
		
	}
	
	public Resource remove(String id,Record currUser) {
		Resource bo=Resource.dao.findById(id);
		bo.set("removed", "1");
		bo.set("updateTime", new Date());
		bo.set("updator", currUser.getStr("id"));
		bo.update();
		return bo;
	}
	
}
