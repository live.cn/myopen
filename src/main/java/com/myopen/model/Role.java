package com.myopen.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.converter.DicConverter;
import com.myopen.util.UUIDUtils;

public class Role extends Model<Role>{

	private static final long serialVersionUID = -2342829214973493652L;
	public static final Role dao = new Role();

	public List<Record> getRecordsByUserId(String userId){
		String sql="select * from t_user_role t where t.userId=?";
		return Db.find(sql,userId);
	}
	
	public Record getRecordById(String id){
		return Db.findById("t_role", id);
	}
	
	private void createTree(Record parent,List<Record> list){
		
		if(StrKit.notBlank(parent.getStr("roleId"))){
			parent.set("checked", true);
		}
		
		List<Record> childs=new ArrayList<>();
		for (Record children : list) {
			if(parent.getStr("id").equals(children.getStr("parentId"))){
				createTree(children,list);
				childs.add(children);
			}
		}
		parent.set("title", parent.getStr("name"));
		parent.set("expand", true);
		if(childs.size()>0){
			//目前这个ivew貌似有bug，貌似childs 数量为0的时候有BUG
			parent.set("children", childs);
		}
	}
	
	public List<Record> getTreeRoleResources(String id){
		String parentSql="select t.*,rr.roleId from t_resource t "
				+ " left join t_role_resource rr on t.id=rr.resourceId and rr.roleId=? "
				+ " where (t.parentId is null or t.parentId='' ) "
				+ " order by t.px ";
		String sql="select t.*,rr.roleId from t_resource t "
				+ " left join t_role_resource rr on t.id=rr.resourceId and rr.roleId=? "
				+ " order by t.px ";
		List<Record> parentList=Db.find(parentSql,id);
		List<Record> list=Db.find(sql,id);
		for (Record parent : parentList) {
			parent.set("title", parent.getStr("name"));
			createTree(parent, list);
		}
		return parentList;
	}
	
	public List<Record> getAll(){
		String sql="select * from t_role t where t.removed='0' ";
		return Db.find(sql);
	}
	
	public Page<Record> pagequery(Record record){
		StringBuffer sqlExceptSelect=new StringBuffer("from t_role t "
				+ " where t.removed='0' ");
		List<Object> list=new ArrayList<Object>();
		
		if(StrKit.notBlank(record.getStr("name"))){
			sqlExceptSelect.append(" and t.name like ? ");
			list.add("%"+record.getStr("name")+"%");
		}
		if(StrKit.notBlank(record.getStr("code"))){
			sqlExceptSelect.append(" and t.code like ? ");
			list.add("%"+record.getStr("code")+"%");
		}
		
		sqlExceptSelect.append(" order by t.createTime desc");
		Page<Record> page= Db.paginate(record.getInt("pageNumber"),record.getInt("pageSize"),"select t.* ",sqlExceptSelect.toString(),list.toArray());		
	
		String[] dicTypeKeys= {"status"};
		String[] dicTypes= {"ROLE_STATUS"};
		
		return new DicConverter<Record>(page,dicTypeKeys,dicTypes).getPage();
	}
	
	public void changeStatus(String id,String status,Record currUser){
		Role role=Role.dao.findById(id);
		role.set("updateTime", new Date());
		role.set("updator", currUser.getStr("id"));
		role.set("status", status);
		role.update();
	}
	
	public Role save(Role vo,Record currUser){
		Role bo=null;
		if(StrKit.isBlank(vo.getStr("id"))){
			bo=new Role();
			
			bo.set("id", UUIDUtils.getUUID());
			bo.set("createTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("status", "ROLE_STATUS_00");//默认可用
			bo.set("comment",vo.getStr("comment"));
			
			bo.save();
		}else{
			bo=Role.dao.findById(vo.getStr("id"));
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("comment",vo.getStr("comment"));

			bo.update();
		}
		
		return bo;
	}
	
	public void updateRoleResource(String roleId,String[] resourceIds,Record currUser){
		String sql="delete from t_role_resource where roleId=? ";
		Db.update(sql,roleId);
		
		if(resourceIds!=null){
			List<Record> list=new ArrayList<>();
			Date date=new Date();
			
			for (String  resourceId : resourceIds) {
				Record bo=new Record();
				bo.set("id", UUIDUtils.getUUID());
				bo.set("createTime", date);
				bo.set("creator", currUser.getStr("id"));
				bo.set("updateTime", date);
				bo.set("updator", currUser.getStr("id"));
				
				bo.set("roleId", roleId);
				bo.set("resourceId", resourceId);
				
				list.add(bo);
			}
			
			Db.batchSave("t_role_resource", list, 500);
		}
		
		
	}
}
