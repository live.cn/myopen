package com.myopen.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.util.UUIDUtils;


/**
 * 由于定时任务要跟当前quartz绑定，所以提供部分方法
 * @author wangsanfei
 *
 */
public class Task extends Model<Task>{

	private static final long serialVersionUID = 5617202869523721885L;
	public static final Task dao=new Task();
	
	private static SchedulerFactory schedulerFactory=new StdSchedulerFactory();
	private static Scheduler scheduler=getScheduler();
	
	
	public Page<Record> pagequery(Record record){
		StringBuffer sqlExceptSelect=new StringBuffer("from t_task t "
				+ " left join t_user u on t.creator=u.id "
				+ " where t.removed='0' ");
		List<Object> list=new ArrayList<Object>();
		
		sqlExceptSelect.append(" order by t.createTime desc");
		return Db.paginate(record.getInt("pageNumber"),record.getInt("pageSize"),"select t.*,u.username,u.nickname,u.email,u.cellphone ",sqlExceptSelect.toString(),list.toArray());		
	}
	
	public List<Record> getAll(){
		String sql="select * from t_task t where t.removed='0' ";
		return Db.find(sql);
	}
	
	public Record getRecordById(String id){
		if(StrKit.isBlank(id)){
			return new Record();
		}
		String sql="select * from t_task t where t.id=? ";
		return Db.findFirst(sql,id);
	}
	
	public CronTrigger createCronTrigger(String trigger,String triggerGroup,String jobCronExp) {
		return TriggerBuilder.newTrigger()
				.withIdentity(trigger, triggerGroup)
				.withSchedule(CronScheduleBuilder.cronSchedule(jobCronExp))
				.startNow()
				.build();
	}
	
	public JobDetail createJobDetail(String job,String jobGroup,Class<? extends Job> jobClass) {
		return JobBuilder.newJob(jobClass)
				.withIdentity(job, jobGroup)
				.build();
	}
	
	@SuppressWarnings("unchecked")
	public Class<? extends Job> createClass(String jobClass){
		try {
			return (Class<? extends Job>)Class.forName(jobClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 初始化一个任务调度器
	 * @return
	 */
	public static Scheduler getScheduler() {
		try {
			return schedulerFactory.getScheduler();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 初始化所有任务
	 */
	public void initAllTask() {
		//查询处，正在执行和暂停中的任务
		String sql="select * from t_task t where t.jobStatus!='2' ";
		List<Record> tasks=Db.find(sql);
		List<Record> errTasks=new ArrayList<>();
		for (Record record : tasks) {
			String job=record.getStr("job");
			String jobGroup=record.getStr("jobGroup");
			String trigger=record.getStr("trigger");
			String triggerGroup=record.getStr("triggerGroup");
			String jobCronExp=record.getStr("cronExpression");
			String jobClass=record.getStr("jobClass");
			
			try {
				registeTask(job, jobGroup, trigger, triggerGroup, jobClass, jobCronExp);
			} catch (Exception e) {
				//此处更新数据库信息
				record.set("jobStatus", "2");//设置为停止执行
				record.set("errInfo", "该任务初始化失败");//
				Db.update("t_task", record);
				
				errTasks.add(record);
			}
		}
		
		//任务初始化完成以后，执行
		startAll();
		
	}
	
	/**
	 * 运行所有任务
	 */
	public void startAll() {
		try {
			scheduler.start();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 关闭所有任务
	 */
	public void stopAll() {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 添加一个任务
	 */
	public void addTask(String job,String jobGroup
			,String trigger,String triggerGroup,String description
			, String jobClass, String jobCronExp,Record currUser) {
		try {
			
			Record bo=new Record();
			bo.set("id", UUIDUtils.getUUID());
			bo.set("createTime", new Date());
			bo.set("updateTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updator", currUser.getStr("id"));
			bo.set("job", job);
			bo.set("jobGroup", jobGroup);
			bo.set("trigger", trigger);
			bo.set("triggerGroup", triggerGroup);
			bo.set("jobStatus", "1");
			bo.set("cronExpression", jobCronExp);
			bo.set("description", description);
			bo.set("jobClass", jobClass);
			Db.save("t_task", bo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 注册一个任务
	 */
	public void registeTask(String job,String jobGroup
			,String trigger,String triggerGroup
			, String jobClass, String jobCronExp) {
		try {
			
			JobDetail jobDetail = createJobDetail(job,jobGroup,createClass(jobClass));
			CronTrigger cronTrigger=createCronTrigger(trigger,triggerGroup,jobCronExp);
			scheduler.scheduleJob(jobDetail, cronTrigger);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 暂停一个任务
	 */
	public void pauseTask(String id) {
		Record task=Db.findById("t_task", id);
		String job=task.getStr("job");
		String jobGroup=task.getStr("jobGroup");
		
		JobKey jobKey=new JobKey(job, jobGroup);
		try {
			
			
			scheduler.pauseJob(jobKey);
			
			//同时数据库状态也更改为暂停
			task.set("jobStatus", "1");
			Db.update("t_task",task);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 恢复一个任务
	 */
	public void resumeTask(String id) {
		
		Record task=Db.findById("t_task", id);
		String job=task.getStr("job");
		String jobGroup=task.getStr("jobGroup");
		
		JobKey jobKey=new JobKey(job, jobGroup);
		try {
			scheduler.resumeJob(jobKey);
			
			//同时数据库状态也更改为正在运行
			task.set("jobStatus", "0");
			Db.update("t_task",task);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除一个任务
	 */
	public void deleteTask(String id) {
		
		Record task=Db.findById("t_task", id);
		String job=task.getStr("job");
		String jobGroup=task.getStr("jobGroup");
		
		JobKey jobKey=new JobKey(job, jobGroup);
		try {
			scheduler.deleteJob(jobKey);
			
			Db.deleteById("t_task", id);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 立刻执行一次任务
	 */
	public void runATask(String id) {
		
		Record task=Db.findById("t_task", id);
		String job=task.getStr("job");
		String jobGroup=task.getStr("jobGroup");
		
		JobKey jobKey=new JobKey(job, jobGroup);
		try {
			scheduler.triggerJob(jobKey);
			
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 更新任务时间表达式
	 */
	public void updateTaskCron(String id,String jobCronExp) {
		
		Record task=Db.findById("t_task", id);
		String job=task.getStr("job");
		String jobGroup=task.getStr("jobGroup");
		String trigger=task.getStr("trigger");
		String triggerGroup=task.getStr("triggerGroup");	
		
		TriggerKey triggerKey = TriggerKey.triggerKey(job,jobGroup);
		try {
			scheduler.rescheduleJob(triggerKey, createCronTrigger(trigger,triggerGroup, jobCronExp));
			
			//同时数据库状态也更改为正在运行
			task.set("cronExpression", jobCronExp);
			Db.update("t_task",task);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		//Task task=new Task();
		//task.registeTask("test", "test", "test", "test", "com.myopen.task.MyTask", "0/10 * * * * ? ");
		//task.startAll();
		
		
	}
	
}
