package com.myopen.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.converter.DicConverter;
import com.myopen.util.UUIDUtils;

public class Website extends Model<Website>{

	private static final long serialVersionUID = 7448817327628715196L;
	public static final Website dao = new Website();
	
	public Page<Record> pagequery(Record record){
		StringBuffer sqlExceptSelect=new StringBuffer("from t_website t "
				+ " left join t_user u on t.userId=u.id "
				+ " where t.removed='0' ");
		List<Object> list=new ArrayList<Object>();
		
		if(StrKit.notBlank(record.getStr("email"))){
			sqlExceptSelect.append(" and u.email like ? ");
			list.add("%"+record.getStr("email")+"%");
		}
		if(StrKit.notBlank(record.getStr("cellphone"))){
			sqlExceptSelect.append(" and u.cellphone like ? ");
			list.add("%"+record.getStr("cellphone")+"%");
		}
		
		sqlExceptSelect.append(" order by t.createTime desc");
		Page<Record> page= Db.paginate(record.getInt("pageNumber"),record.getInt("pageSize"),"select t.*,u.email,u.cellphone,u.nickname ",sqlExceptSelect.toString(),list.toArray());		
		String[] dicTypeKeys= {"status"};
		String[] dicTypes= {"WEBSITE_STATUS"};
		
		return new DicConverter<Record>(page,dicTypeKeys,dicTypes).getPage();
	}
	
	public Record getRecordById(String id){
		if(StrKit.isBlank(id)){
			return new Record();
		}
		String sql="select * from t_website t where t.id=? ";
		return Db.findFirst(sql,id);
	}
	
	public Website save(Website vo,Record currUser){
		Website bo=null;
		if(StrKit.isBlank(vo.getStr("id"))){
			bo=new Website();
			
			bo.set("id", UUIDUtils.getUUID());
			bo.set("username", vo.getStr("username"));
			bo.set("createTime", new Date());
			bo.set("updateTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updator", currUser.getStr("id"));
			
			bo.save();
		}else{
			bo=Website.dao.findById(vo.getStr("id"));
			
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			bo.set("name", vo.getStr("name"));

			bo.update();
		}
		
		return bo;
	}
	
	public void changeStatus(String id,String status,Record currUser){
		Website bo=Website.dao.findById(id);
		bo.set("updateTime", new Date());
		bo.set("updator", currUser.getStr("id"));
		bo.set("status", status);
		bo.update();
	}
	
	public Website remove(String id,Record currUser) {
		Website bo=Website.dao.findById(id);
		bo.set("removed", "1");
		bo.set("updateTime", new Date());
		bo.set("updator", currUser.getStr("id"));
		bo.update();
		return bo;
	}
	
	public Record getMyWebsite(String userId) {
		String sql="select * from t_website t where t.userId=? ";
		Record bo= Db.findFirst(sql,userId);
		if(bo==null) {
			bo=new Record();
			Record currUser=User.dao.getRecordById(userId);
			bo.set("id", UUIDUtils.getUUID());
			bo.set("createTime", new Date());
			bo.set("updateTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updator", currUser.getStr("id"));
			bo.set("updator", currUser.getStr("id"));
			bo.set("userId", currUser.getStr("id"));
			bo.set("status", "0");
			bo.set("sld", UUIDUtils.getUUID());
			Db.save("t_website", bo);
		}
		return bo;
	}
	
}
