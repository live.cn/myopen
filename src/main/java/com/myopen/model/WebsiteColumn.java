package com.myopen.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.converter.DicConverter;
import com.myopen.util.UUIDUtils;

public class WebsiteColumn extends Model<WebsiteColumn>{

	private static final long serialVersionUID = 6223784462221857824L;
	public static final WebsiteColumn dao = new WebsiteColumn();
	
	public Page<Record> pagequery(Record record){
		StringBuffer sqlExceptSelect=new StringBuffer("from t_website_column t "
				+ " left join t_user u on t.userId=u.id "
				+ " where t.removed='0' ");
		List<Object> list=new ArrayList<Object>();
		
		if(StrKit.notBlank(record.getStr("email"))){
			sqlExceptSelect.append(" and u.email like ? ");
			list.add("%"+record.getStr("email")+"%");
		}
		if(StrKit.notBlank(record.getStr("cellphone"))){
			sqlExceptSelect.append(" and u.cellphone like ? ");
			list.add("%"+record.getStr("cellphone")+"%");
		}
		
		sqlExceptSelect.append(" order by t.createTime desc");
		Page<Record> page= Db.paginate(record.getInt("pageNumber"),record.getInt("pageSize"),"select t.*,u.email,u.cellphone,u.nickname ",sqlExceptSelect.toString(),list.toArray());		
		String[] dicTypeKeys= {"status"};
		String[] dicTypes= {"WEBSITE_STATUS"};
		
		return new DicConverter<Record>(page,dicTypeKeys,dicTypes).getPage();
	}
	
	public Record getRecordById(String id){
		return Db.findById("t_website_column", id);
	}
	
	private void getChild(Record parent,List<Record> list){
		List<Record> childs=new ArrayList<Record>();
		for (Record record : list) {
			if(parent.getStr("id").equals(record.getStr("parentId"))){
				
				record.set("spread", true);
				
				getChild(record,list);
				childs.add(record);
			}
		}
		
		if(childs.size()>0){
			parent.set("children", childs);
		}
		
	}
	
	
	public List<Record> getTree(){
		String psql="select * from t_website_column t where (t.parentId is null or t.parentId='') and t.removed='0' order by t.px ";
		String lsql="select * from t_website_column t where 1=1 and t.removed='0' order by t.px";
		List<Record> parents=Db.find(psql);
		List<Record> list=Db.find(lsql);
		
		for (Record record : parents) {
			
			record.set("spread", true);
			
			getChild(record,list);
		}
		
		return parents;
	}
	
	public List<Record> getTree(String module){
		
		if(StrKit.isBlank(module)) {
			return getTree();
		}
		
		String psql="select * from t_website_column t where (t.parentId is null or t.parentId='') and t.removed='0' and t.module=? order by t.px ";
		String lsql="select * from t_website_column t where 1=1 and t.removed='0' and t.module=? order by t.px";
		List<Record> parents=Db.find(psql,module);
		List<Record> list=Db.find(lsql,module);
		
		for (Record record : parents) {
			
			record.set("spread", true);
			
			getChild(record,list);
		}
		
		return parents;
	}
	
	public WebsiteColumn save(WebsiteColumn vo,Record currUser){
		
		WebsiteColumn bo=null;
		if(StrKit.isBlank(vo.getStr("id"))){
			bo=new WebsiteColumn();
			
			bo.set("id", UUIDUtils.getUUID());
			bo.set("createTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("px", vo.getInt("px"));
			bo.set("parentId", vo.getStr("parentId"));
			bo.set("comment", vo.getStr("comment"));
			bo.set("module", vo.getStr("module"));
			
			bo.save();
		}else{
			bo=WebsiteColumn.dao.findById(vo.getStr("id"));
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			
			bo.set("name", vo.getStr("name"));
			bo.set("code", vo.getStr("code"));
			bo.set("px", vo.getInt("px"));
			bo.set("parentId", vo.getStr("parentId"));
			bo.set("comment", vo.getStr("comment"));
			bo.set("module", vo.getStr("module"));

			bo.update();
		}
		
		return bo;
		
	}
	
	public Resource remove(String id,Record currUser) {
		Resource bo=Resource.dao.findById(id);
		bo.set("removed", "1");
		bo.set("updateTime", new Date());
		bo.set("updator", currUser.getStr("id"));
		bo.update();
		return bo;
	}
}
