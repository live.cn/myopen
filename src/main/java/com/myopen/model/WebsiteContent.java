package com.myopen.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.converter.DicConverter;
import com.myopen.util.UUIDUtils;

public class WebsiteContent extends Model<WebsiteContent>{

	private static final long serialVersionUID = 3180656849023425191L;
	public static final WebsiteContent dao = new WebsiteContent();
	
	public Page<Record> pagequery(Record record){
		StringBuffer sqlExceptSelect=new StringBuffer("from t_website_content t "
				+ " left join t_website w on t.websiteId=w.id"
				+ " left join t_website_column c on t.websiteColumnId=c.id "
				+ " where t.removed='0' ");
		List<Object> list=new ArrayList<Object>();
		
		if(StrKit.notBlank(record.getStr("email"))){
			sqlExceptSelect.append(" and u.email like ? ");
			list.add("%"+record.getStr("email")+"%");
		}
		if(StrKit.notBlank(record.getStr("cellphone"))){
			sqlExceptSelect.append(" and u.cellphone like ? ");
			list.add("%"+record.getStr("cellphone")+"%");
		}
		
		sqlExceptSelect.append(" order by t.createTime desc");
		Page<Record> page= Db.paginate(record.getInt("pageNumber"),record.getInt("pageSize"),"select t.*,w.name websiteName,c.name websiteColumnName",sqlExceptSelect.toString(),list.toArray());		
		String[] dicTypeKeys= {"module","status"};
		String[] dicTypes= {"CMS_MODULE","CMS_CONTENT_STATUS"};
		
		return new DicConverter<Record>(page,dicTypeKeys,dicTypes).getPage();
	}
	
	public void save(WebsiteContent vo,Record currUser) {
		WebsiteContent bo=null;
		if(StrKit.isBlank(vo.getStr("id"))){
			bo=new WebsiteContent();
			
			bo.set("id", UUIDUtils.getUUID());
			bo.set("removed", "0");
			bo.set("createTime", new Date());
			bo.set("updateTime", new Date());
			bo.set("creator", currUser.getStr("id"));
			bo.set("updator", currUser.getStr("id"));
			bo.set("websiteId", vo.getStr("websiteId"));
			bo.set("websiteColumnId", vo.getStr("websiteColumnId"));
			bo.set("module", vo.getStr("module"));
			bo.set("status", "CMS_CONTENT_STATUS_00");
			bo.set("title", vo.getStr("title"));
			bo.set("attribute", vo.getStr("attribute"));
			bo.set("pv", 0);
			
			bo.save();
		}else {
			bo=WebsiteContent.dao.findById(vo.getStr("id"));
			
			bo.set("updateTime", new Date());
			bo.set("updator", currUser.getStr("id"));
			bo.set("title", vo.getStr("title"));
			bo.set("attribute", vo.getStr("attribute"));
			
			bo.save();
		}
	}
	
}
