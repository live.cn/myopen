package com.myopen.model;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.myopen.converter.DicConverter;

public class WebsitePage extends Model<WebsitePage>{

	private static final long serialVersionUID = -127003547490533686L;
	public static final WebsitePage dao = new WebsitePage();
	
	public Page<Record> pagequery(Record record){
		StringBuffer sqlExceptSelect=new StringBuffer("from t_website_page t "
				+ " left join t_website w on t.websiteId=w.id"
				+ " left join t_user u on w.userId=u.id "
				+ " where t.removed='0' ");
		List<Object> list=new ArrayList<Object>();
		
		if(StrKit.notBlank(record.getStr("email"))){
			sqlExceptSelect.append(" and u.email like ? ");
			list.add("%"+record.getStr("email")+"%");
		}
		if(StrKit.notBlank(record.getStr("cellphone"))){
			sqlExceptSelect.append(" and u.cellphone like ? ");
			list.add("%"+record.getStr("cellphone")+"%");
		}
		
		sqlExceptSelect.append(" order by t.createTime desc");
		Page<Record> page= Db.paginate(record.getInt("pageNumber"),record.getInt("pageSize"),"select t.*,u.email,u.cellphone,u.nickname ",sqlExceptSelect.toString(),list.toArray());		
		String[] dicTypeKeys= {"status"};
		String[] dicTypes= {"WEBSITE_STATUS"};
		
		return new DicConverter<Record>(page,dicTypeKeys,dicTypes).getPage();
	}
	
}
