package com.myopen.route;

import com.jfinal.config.Routes;
import com.myopen.controller.admin.AdminController;
import com.myopen.controller.admin.DicController;
import com.myopen.controller.admin.LogController;
import com.myopen.controller.admin.MyWebsiteController;
import com.myopen.controller.admin.ResourceController;
import com.myopen.controller.admin.RoleController;
import com.myopen.controller.admin.TaskController;
import com.myopen.controller.admin.UserController;
import com.myopen.controller.admin.WebsiteColumnController;
import com.myopen.controller.admin.WebsiteContentController;
import com.myopen.controller.admin.WebsiteController;
import com.myopen.controller.admin.WebsiteMaterialController;
import com.myopen.controller.admin.WebsitePageController;
import com.myopen.interceptor.AdminInterceptor;

public class AdminRoutes extends Routes{

	@Override
	public void config() {
		
		addInterceptor(new AdminInterceptor());
		
		add("/admin", AdminController.class);
		add("/admin/resource", ResourceController.class);
		add("/admin/user", UserController.class);
		add("/admin/role", RoleController.class);
		add("/admin/log", LogController.class);
		add("/admin/dic", DicController.class);
		add("/admin/task", TaskController.class);
		
		add("/admin/website", WebsiteController.class);
		add("/admin/websiteMaterial", WebsiteMaterialController.class);
		add("/admin/websiteColumn", WebsiteColumnController.class);
		add("/admin/websiteContent", WebsiteContentController.class);
		add("/admin/websitePage", WebsitePageController.class);
		
		add("/admin/myWebsite", MyWebsiteController.class);
	}

}
