package com.myopen.route;

import com.jfinal.config.Routes;
import com.myopen.controller.IndexController;

public class FrontRoutes extends Routes{

	@Override
	public void config() {
		add("/", IndexController.class);
	}

}
