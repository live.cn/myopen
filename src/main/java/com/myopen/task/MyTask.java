package com.myopen.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class MyTask implements Job{

	public static int a=0;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("我执行了"+(++a)+"次");
	}

}
