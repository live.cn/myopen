package com.myopen.validator;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.myopen.base.BaseVo;
import com.myopen.config.ErrorConfig;

public class MyWebsiteValidator implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		
		Controller c=inv.getController();
		
		BaseVo baseVo=new BaseVo();
		boolean flag=true;
		
		String name=c.getPara("name");
		
		if(StrKit.isBlank(name)) {
			baseVo.setCode(ErrorConfig.err_11001.getCode());
			baseVo.setMsg(ErrorConfig.err_11001.getMsg());
			flag=false;
		}else if(StrKit.isBlank(name)) {
			
		}
		
		if(flag) {
			inv.invoke();
		}else {
			c.renderJson(baseVo);
		}
		
	}

}
