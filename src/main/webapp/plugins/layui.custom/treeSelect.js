/*
 * 扩展一个treeSelect模块，联动Select组件
 * 
 * */

layui.define(['form','jquery'],function(exports) { //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
	"use strict";
	
	var $ = layui.$;
	
	var layuiTreeSelectCalss="layui-treeSelect";
	
	var getSelectDiv=function(nodes,id,layFilter,value){
		var div="<div class='layui-input-inline "+layuiTreeSelectCalss+"' style='width:120px' id='"+id+"'>";
		div+="<select lay-filter='"+layFilter+"'>";
		div+="<option value=''>---请选择---</option>";
		for(var i=0;i<nodes.length;i++){
			if(value){
				div+="<option value='"+nodes[i].id+"' "+(nodes[i].id==value?'selected':'')+" >"+nodes[i].name+"</option>";
			}else{
				div+="<option value='"+nodes[i].id+"' "+""+" >"+nodes[i].name+"</option>";
			}
			
		}
		div+="</select>";
		div+="</div>";
		return div;
	}
	
	var initTreeSelect=function(nodes,container,value){
		
		var eleId=new Date().getTime()+""+parseInt(1000*Math.random());
		var layFilter=eleId+"_layFilter";
		
		var selectHtml=getSelectDiv(nodes,eleId,layFilter,value);
		$(container).append(selectHtml);
		
		layui.form.render();
		
		layui.form.on('select('+layFilter+')', function(data){
			
			//显示删除当前节点后面的节点
			$("#"+eleId).nextAll("div."+layuiTreeSelectCalss).remove();
			
			for(var i=0;i<nodes.length;i++){
				if(nodes[i].id==data.value){
					var children=nodes[i].children;
					if(children&&children.length>0){
						initTreeSelect(children,container);
					}
					break;
				}
			}
		}); 
		
	}
	
	var treeSelect={
			getSingleData:function(nodes,data,tempData){ //外部用不到，用于内部递归
				if(nodes.length==undefined||nodes.length==0){
					return false;
				}
				
				for(var i=0;i<nodes.length;i++){
					if(nodes[i].id==data){
						tempData.push(nodes[i].id);
						return true;
					}else{
						tempData.push(nodes[i].id);
						if(this.getSingleData(nodes[i].children,data,tempData)){
							return true;
						};
					}
					tempData.pop();
				}
			},
			setSelect:function(nodes,container,data){ //外部用不到，用于内部递归
				
				if(data.length>0){
					
					if(nodes==undefined||nodes.length==0){
						return false;
					}
					
					for(var i=0;i<nodes.length;i++){
						
						if(nodes[i].id==data[0]){
							
							initTreeSelect(nodes,container,data[0]);
							var children=nodes[i].children;
							
							if(data.length>1){
								data.shift();
								this.setSelect(children,container,data);
								break;
							}
						}
					}
					
				}
			},
			init:function(container,param){//初始化组件，目前param，nodes（树形JSON）,data设置组件的值
				if(!this.cacheData){
					this.cacheData={};
				}
				this.cacheData[container]={};
				
				this.cacheData[container].nodes=param.nodes;
				
				$(container).find("div."+layuiTreeSelectCalss).remove();
				initTreeSelect(this.cacheData[container].nodes,container);
				if(param.data){
					this.setValue(container,param.data);
				}
			},
			setValue:function(container,data){//设置组件的值，data可以为string和数组，如果是string，则认为最后以及的值，反向推算。如果是数组，则按照数组进行赋值
				
				var nodes=this.cacheData[container].nodes;
				
				if(typeof data=='string'){
					//如果是string，那么说明传的是最后一个值，从nodes中找出对应的数组
					var tempData=[];
					
					for(var i=0;i<nodes.length;i++){
						if(nodes[i].id==data){
							tempData.push(nodes[i].id);
							break;
						}else{
							tempData.push(nodes[i].id);
							if(nodes[i].childre){
								if(this.getSingleData(nodes[i].children,data,tempData)){
									break;
								}
							}
						}
						tempData.pop();
					}
					data=tempData;
				}
				
				$(container).find("div."+layuiTreeSelectCalss).remove();
				this.setSelect(nodes,container,data);
				
			},
			getTreeData:function(container){//获取当前组件的所有数据
				return this.cacheData[container].nodes;
			},
			getData:function(container){//获取当前组件的值，是个数组
				var data=[];
				$(container).find("div."+layuiTreeSelectCalss).find("select").each(function(){
					data.push($(this).val());
				});
				return data;
			},
			
	};
	
	//输出treeSelect接口
	exports('treeSelect', treeSelect);
});